﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speechManager : MonoBehaviour
{
    public GameObject m_speech;
    public int textDelay;


    // Start is called before the first frame update
    void Update()
    {
        //closes text boxes
        if (Input.GetKeyDown(KeyCode.Q))
        {
            m_speech.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //makes text box appear
        if (other.gameObject.CompareTag("speech"))
        {
            Invoke("Visible", textDelay);

        }

        if (other.gameObject.CompareTag("speech2"))
        {
            Invoke("Visible", textDelay);

        }

        if (other.gameObject.CompareTag("speech3"))
        {
            Invoke("Visible", textDelay);

        }

        if (other.gameObject.CompareTag("speech4"))
        {
            Invoke("Visible", textDelay);

        }

        if (other.gameObject.CompareTag("speech5"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech6"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech7"))
        {
            Invoke("Visible", textDelay);

        
        }
        if (other.gameObject.CompareTag("speech8"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech9"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech 10"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech 11"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech 12"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech 13"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech 14"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech 15"))
        {
            Invoke("Visible", textDelay);

        }
        if (other.gameObject.CompareTag("speech 16"))
        {
            Invoke("Visible", textDelay);

        }

    }

    void Visible()
    {
        //text box appearing
        print("apear");
        m_speech.SetActive(true);
    }
}
