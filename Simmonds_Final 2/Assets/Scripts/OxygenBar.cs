﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OxygenBar : MonoBehaviour
{

    public float m_StartingHealth = 100f;        
    public Slider m_Slider;                   
    public Image m_FillImage;                         
    public Color m_FullHealthColor = Color.green;      
    public Color m_ZeroHealthColor = Color.red;
    public TextMeshProUGUI death_words;
    public GameObject death_screen;

    public int drainDamage;
    public int drainTime;
    public int drainDelay;
    public int gainAmount;
    public int gainTime;
    public int deathdelay;

    private float m_CurrentHealth;                   
    public bool m_Dead;
    public bool O2gain;


    private void Awake()
    {
        //O2 drain
        InvokeRepeating("Drain", drainTime, drainDelay);
    }


    private void OnEnable()
    {
        //checks that the player starts with the correct health and makes to player not dead
        m_CurrentHealth = m_StartingHealth;
        m_Dead = false;

        SetHealthUI();
    }

    private void Update()
    {
        //prevents the player for aquiring infinte health
        if (m_CurrentHealth >= 150f)
        {
            m_CurrentHealth = 150f;
        }
        //calls the function to gain o2
        if (O2gain)
        {
            Invoke("Gain", gainTime);
        }
    }
    public void TakeDamage(float amount)
    {
        //deals damage to players
        m_CurrentHealth -= amount;

        SetHealthUI();
        //checks for player death due to o2
        if (m_CurrentHealth <= 0f && !m_Dead)
        {
            OnDeath();
        }
    }


    void Drain()
    {
        //this is the drain damage, the slider is being updated with the damage and it also checks for player death
        m_CurrentHealth -= drainDamage;
        m_Slider.value = m_CurrentHealth;
        if (m_CurrentHealth <= 0 && !m_Dead)
        {
            OnDeath();
        }
    }

    private void SetHealthUI()
    {
        // this updates how the slider looks on screen
        m_Slider.value = m_CurrentHealth;

        m_FillImage.color = Color.Lerp(m_ZeroHealthColor, m_FullHealthColor, m_CurrentHealth / m_StartingHealth);
    }


    private void OnDeath()
    {
        //this makes the player dead and triggers the death sequence
        m_Dead = true;
        print("dead");
        Invoke("DeathText", deathdelay);
        death_screen.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        //this is to check when the player enters the o2 bubbles
        if (other.gameObject.CompareTag("O2bubble"))
        {
            O2gain = true;
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //this is to check when players leave the bubble
        if (other.gameObject.CompareTag("O2bubble"))
        {
            O2gain = false;
        }
    }

    void Gain()
    {
        //this tells how much o2 to add and updates the slider
        m_CurrentHealth += gainAmount;
        m_Slider.value = m_CurrentHealth;
    }

    void DeathText()
    {
        //death text
        death_words.text = "March 13th, 2047\n\n#24601\n\nFargo, ND\n\nTest subject 24601, codename Sully, perished from oxygen deprivation. Subject also showed early signs of depresion, but nothing conclusive. For next test tweak the levels of depresants in oxygen supply. Also montitor heatrate to better test depression of future subjects.";
    }
}