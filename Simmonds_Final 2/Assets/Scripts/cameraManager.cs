﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class cameraManager : MonoBehaviour
{
    public GameObject Camera;
    public GameObject Cinematic;
    public int camPosition = 1;
    public TextMeshProUGUI m_Text;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //calling for the camera switch
        switchCamera();

    }
    public void switchCamera()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            //this keeps track of which camera is active as well as rotating the text so the player can always read it
            cameraPositionCounter();
            camPosition += 1;
            m_Text.transform.Rotate(0, 180, 0);

        }
    }

    void cameraPositionCounter()
    {
        //this checks which camera is supposed to be active and properly activates the right one
        if (camPosition > 1)
        {
            camPosition = 0;
        }

        if (camPosition == 1)
        {
            Camera.SetActive(true);

            Cinematic.SetActive(false);


        }

        if (camPosition == 0)
        {
            Camera.SetActive(false);

            Cinematic.SetActive(true);

        }
    }
}
