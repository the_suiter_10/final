﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour
{
    string sceneName;
    public int scenenumber;
    // Start is called before the first frame update
    void Start()
    {
        //refencing scene
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey("r"))
        {
            //finds current scene and restarts from title screen by pressing "r"
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - scenenumber);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //moves to next scene when player collides
        if (other.gameObject.CompareTag("Teleporter+"))
        {
            print("+");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        }
        //unused but does opposite
        if (other.gameObject.CompareTag("Teleporter-"))
        {
            print("-");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

        }

    }
}