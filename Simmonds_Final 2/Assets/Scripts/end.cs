﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class end : MonoBehaviour
{
    public TextMeshProUGUI final;
    public AudioSource clip;
    public int fDelay;
    public int gDelay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //last scene script amnager, helps start the scene
        Invoke("Gunshot", gDelay);
        Invoke("Finaltext", fDelay);
    }
    void Gunshot()
    {
        //plays gunshot audio
        clip.Play();
        print("play");
    }

    void Finaltext()
    {
        //end text
        final.text = "March 2nd, 2047\n\n#A113\n\nAyo, WA\n\nTest subject TEO319, codename Sully, was gunned down after escaping the facility. He did not succumb to depression like most other test subjects. More data must be recorded. Is Sully an anomaly, or can we expect to see more like Sully in the future?";
    }
}
