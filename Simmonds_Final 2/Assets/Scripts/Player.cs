using UnityEngine;
using System.Collections;
using System;
using TMPro;

public class Player : MonoBehaviour
{

    private Animator anim;
    private CharacterController controller;
    private Rigidbody rb;
    public TextMeshProUGUI death_text;
    public GameObject screen;
    public int textdelay;
    public GameObject slider;

    public float speed = 600.0f;
    public float turnSpeed = 0.0f;
    private Vector3 moveDirection = Vector3.zero;
    public float gravity = 20.0f;
    public float JumpSpeed = 7.0f;
    int floorMask;
    public int grounded = 0;

    void Start()
    {
        //referencing stuff
        controller = GetComponent<CharacterController>();
        anim = gameObject.GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
        floorMask = LayerMask.GetMask("Ground");

    }
    void Update()
    {
        //This triggers the players animations as well as player movement
        if (Input.GetKey("w"))
        {
            anim.SetInteger("AnimationPar", 1);
        }
        
        else if (Input.GetKey("s"))
        {
            anim.SetInteger("AnimationPar", 1);
        }
        else
        {
            anim.SetInteger("AnimationPar", 0);
        }

        if (controller.isGrounded)
        {
            moveDirection = transform.forward * Input.GetAxis("Vertical") * speed;
            moveDirection = transform.forward * moveDirection.magnitude;
            moveDirection *= speed;
        }

        //movement
        float turn = Input.GetAxis("Horizontal");
        transform.Rotate(0, turn * turnSpeed * Time.deltaTime, 0);
        controller.Move(moveDirection * Time.deltaTime);
        moveDirection.y -= gravity * Time.deltaTime;

    }

    private void FixedUpdate()
    {
        //if on ground player can jump & jumping script
        if (Input.GetKeyDown(KeyCode.Space) && (grounded == 1))
        {
            anim.SetBool("IsFloating", true);

            grounded = 0;

            rb.AddForce(Vector3.up * JumpSpeed, ForceMode.Impulse);

            moveDirection.y = JumpSpeed;

            controller.Move(moveDirection * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //checks to see if player is on ground
        if (other.gameObject.CompareTag("Ground"))
        {
            anim.SetBool("IsFloating", false);
            grounded = 1;
        }
        //triggers the death screen
        if (other.gameObject.CompareTag("Dead"))
        {
            screen.SetActive(true);
            slider.SetActive(false);
            Invoke("Text", textdelay);
        }
    }
    void Text()
    {
        //death text
        death_text.text = "May 9th, 2047\n\n#A113\n\nSacremento, CA\n\nTest subject A113, codename Sully, jumpED into the void. Likely cause of death was depression, but further tests will be needed. If the test was succesful we will go forward with stage three of project Ferrell. The future looks promising.";
    }
}