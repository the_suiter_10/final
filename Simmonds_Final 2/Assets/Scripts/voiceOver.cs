﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voiceOver : MonoBehaviour
{
    public BoxCollider m_box;
    public AudioSource voice;
    public int counter;
    // Start is called before the first frame update
    void Start()
    {
        //text counter
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //plays the voice overs I made and also ensures that they don't play more than once
        if (counter == 0)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                voice.Play();

                counter = 1;
            }
        }
    }
}
